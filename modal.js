function Modal(message) {
  return new Promise((resolve, reject) => {
    const body = document.getElementsByTagName("body")[0];
    const root = document.createElement("div");
    const overlay = document.createElement("div");
    const messageContainer = document.createElement("div");
    const messageContent = document.createTextNode(message);
    const buttonsContainer = document.createElement("div");
    const confirmButton = document.createElement("button");
    const cancelButton = document.createElement("button");

    function constructModal() {
      messageContainer.appendChild(messageContent);
      root.appendChild(messageContainer);

      confirmButton.innerHTML = "Yes";
      cancelButton.innerHTML = "No";

      buttonsContainer.appendChild(confirmButton);
      buttonsContainer.appendChild(cancelButton);
      root.appendChild(buttonsContainer);

      root.classList.add("modalContainer");
      overlay.classList.add("overlay");
      messageContainer.classList.add("message");
      confirmButton.classList.add("button");
      cancelButton.classList.add("button");
      confirmButton.classList.add("yes");
      cancelButton.classList.add("no");
    }

    function mountModal() {
      confirmButton.addEventListener("click", handleConfirmation);
      cancelButton.addEventListener("click", handleCancelation);
      body.appendChild(overlay);
      body.appendChild(root);
    }

    function unmountModal() {
      confirmButton.removeEventListener("click", handleConfirmation);
      cancelButton.removeEventListener("click", handleCancelation);
      body.removeChild(root);
      body.removeChild(overlay);
    }

    function handleConfirmation() {
      unmountModal();
      resolve("yes");
    }

    function handleCancelation() {
      unmountModal();
      reject("no");
    }

    constructModal();
    mountModal();
  });
}
